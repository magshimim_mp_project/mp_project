package com.example.dor71.mp_project;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Looper;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;

/**
 * Created by dor71 on 2/14/2017.
 */

/*utility class, all static, does miscellaneous stuff*/
public abstract class Utility {

    /*a sorting subclass*/
    public static class Sorter
    {
        /*sort playlist by alphabetical order of song titles*/
        public static void alpha_sort_by_name(PlayList list){
            Collections.sort(list.get_data(), new Comparator<Song>() {
                @Override
                public int compare(Song song1, Song song2){
                    return song1.get_title().compareTo(song2.get_title());
                }
            });
        }
        /*sort playlist by alphabetical order of song artist*/
        public static void alpha_sort_by_artist(PlayList list){
            Collections.sort(list.get_data(), new Comparator<Song>() {
                @Override
                public int compare(Song song1, Song song2){
                    return song1.get_artist().compareTo(song2.get_artist());
                }
            });
        }
        /*sort playlist by alphabetical order of song year*/
        public static void alpha_sort_by_year(PlayList list){
            Collections.sort(list.get_data(), new Comparator<Song>() {
                @Override
                public int compare(Song song1, Song song2){
                    return song1.get_year().compareTo(song2.get_year());
                }
            });
        }
        /*sort playlist by alphabetical order of song titles*/
        public static void alpha_sort_by_album(PlayList list){
            Collections.sort(list.get_data(), new Comparator<Song>() {
                @Override
                public int compare(Song song1, Song song2){
                    return song1.get_album().compareTo(song2.get_album());
                }
            });
        }
    }

    /*a searcher subclass*/
    public static class Locator{
        /*returns the next song in the playlist*/
        public static Song get_next_song(PlayList pls, Song s) {
            try{
                return pls.get_data().get(pls.get_data().indexOf(s) + 1);
            }catch(NullPointerException e)
            {
                e.printStackTrace();
            }
            return null;
        }
        /*returns the previous song in the playlist*/
        public static Song get_previous_song(PlayList pls, Song s) {
            try{
                return pls.get_data().get(pls.get_data().indexOf(s) - 1);
            }catch(NullPointerException e)
            {
                e.printStackTrace();
            }
            return null;
        }
        /*returns the song that the given id belongs to (from the given playlist) if not found will return NULL*/
        public static Song get_song_by_id(PlayList pls, long id){
            for(int i = 0; i < pls.get_data().size(); i++)
            {
                if(pls.get_data().get(i).get_id() == id) return pls.get_data().get(i);
            }
            return null;
        }
    }

    /*global constants*/
    public static class Constants{
        public static final int KEEP_SAME = 3000; //used to keep some values as they were when passing arguments to database write functions.
    }
    /*public static class Guard{

        //makes a toast!
        public static void cheers(Activity activity, String str){
            final Toast toast = Toast.makeText(activity.getApplicationContext(), str, Toast.LENGTH_LONG);
            toast.show();
        }
    }*/

}
