package com.example.dor71.mp_project;

import android.content.ContentUris;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by dor71 on 1/13/2017.
 */

/*will control and manage all of the media actions*/
public class MediaController {

    private GUI _gui;
    private PlayList _main_song_list; // the main song list (in other words all of the relevant audio on the device)
    private Song _curr_song;
    private MediaPlayer _mp;
    private int _curr_song_pos; // the current position in the played song, NOTE: only updates during pause and resume so not efficient for realtime actions

    /*getters*/
    public PlayList get_main_song_list() {
        return _main_song_list;
    }

    public Song get_curr_song() {
        return _curr_song;
    }

    public int getCurr_song_pos() {
        return _curr_song_pos;
    }

    protected MediaPlayer get_mp(){return _mp;}

    public boolean isPlaying()
    {
        return _mp.isPlaying();
    }

    /*constructor*/
    public MediaController(ArrayList<Song> main_song_list, GUI gui) {
        _mp = new MediaPlayer();
        _main_song_list = new PlayList(main_song_list, 0, "");
        _curr_song_pos = 0;
        _curr_song = null; //TODO: make currSong resume to the last played song even before opening the application (save it in the DB and resume to it)
        _gui = gui;

        /*listener that will trigger whenever a song ends*/
        _mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
            @Override
            public void onCompletion(MediaPlayer mp){
                /*is shuffle checkbox is checked, shuffle a random song a play it, else play the next song in the playlist*/
                if (_gui.get_shuffle_checkbox().isChecked()) {
                    play_rand_song();
                } else {
                    play_next(); //play the next song
                }
                _gui.get_seeker().setProgress(0); // reset seekbar
            }
        });
    }

    public void play_rand_song(){
        //generate a random number
        Random rand = new Random();
        int random_num = rand.nextInt(_main_song_list.get_data().size());
        System.out.println("random number: " + random_num);

        //play the song in the position of the random number
        play_song(_main_song_list.get_data().get(random_num), _gui.get_activity());
    }

    /*plays a given song*/
    public void play_song(Song s, android.content.Context c) {
        _mp.reset(); // reseting player to avoid errors
        _curr_song_pos = 0; // reseting song's playing position
        try {

            Uri trackUri = ContentUris.withAppendedId(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, s.get_id()); //takes the URI path and adds the song's id to the end of it
            _mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
            _mp.setDataSource(c, trackUri);
            _mp.prepare();
            _mp.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        _curr_song = s;


        //setting the song title text view  to the new song's title.
        _gui.get_curr_song_title().setText(s.get_title());
    }

    /*will play next song in playlist*/
    public void play_next(){
        play_song(_main_song_list.get_next_song(_curr_song), _gui.get_activity().getApplicationContext());
    }

    /*will play previous song in playlist*/
    public void play_prev(){
        play_song(_main_song_list.get_previous_song(_curr_song), _gui.get_activity().getApplicationContext());
    }

    /*seek to given position*/
    protected void seekto(int pos)
    {
        _mp.seekTo(pos);
    }

    /*pause current song*/
    protected void pause() {
        //checking if player is already playing
        if (_mp.isPlaying()) {
            _mp.pause(); // pause player
            _curr_song_pos = _mp.getCurrentPosition();
        }
    }

    /*resume last played song*/
    protected void resume() {
        // if the player isn't playing any song currently
        if (_mp == null) {
            try{
                _mp.seekTo(_curr_song_pos);
            }catch(NullPointerException e){
                e.printStackTrace();
            }
        }
        //play!
        _mp.start();
    }
}