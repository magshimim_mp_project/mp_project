package com.example.dor71.mp_project;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.net.Uri;
import android.provider.MediaStore;

import java.security.Permission;
import java.util.ArrayList;

import static android.content.Context.MODE_ENABLE_WRITE_AHEAD_LOGGING;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by dor71 on 1/1/2017.
 */

/*will manage the databases: will communicate with the built-in database of the device and with the front database*/
public abstract class DatabaseManager {

    private static DatabaseManager.FrontManager.Writer instance = null;
    private static PlayList _mainlist = null;


    /*static class, manages back database (read only)*/
    public static class BackManager extends DatabaseManager{

        /*will return the complete list of songs on device*/
        public static PlayList get_songs(ContentResolver musicResolver) {
            //if didn't query the database yet, do so now, then return the _mainlist
            if(_mainlist == null) query_for_songs(musicResolver);
            return _mainlist;
        }

        /*will read from the device's built in database*/
        private static void query_for_songs(ContentResolver musicResolver){
            ArrayList<Song> songList = new ArrayList<>();
            Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);

            if (musicCursor != null && musicCursor.moveToFirst()) {
                //get columns
                int titleColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media.TITLE);
                int idColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media._ID);
                int artistColumn = musicCursor.getColumnIndex(android.provider.MediaStore.Audio.Media.ARTIST);

                //add songs to list
                do {
                    long thisId = musicCursor.getLong(idColumn);
                    String thisTitle = musicCursor.getString(titleColumn);
                    String thisArtist = musicCursor.getString(artistColumn);

                    songList.add(new Song(thisId, thisTitle, thisArtist, "", "", null, null));

                } while (musicCursor.moveToNext());
            }
            try {
                musicCursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            _mainlist = new PlayList(songList, 1, "main_list");
        }
    }

    /*manages front database*/
    public class FrontManager extends DatabaseManager{

        SQLiteDatabase _database;
        SQLiteDatabase.CursorFactory _cursorFactory;

        /*data fields*/
        ArrayList<String> _titles, _artists, _years, _genres;
        PlayList _mainlist;
        ArrayList<Integer> albums_id, playlists_id;

        /*constructor*/
        public FrontManager(){
            //TODO:: FrondManager constructor
            _cursorFactory = new SQLiteDatabase.CursorFactory() {
                @Override
                public Cursor newCursor(SQLiteDatabase db, SQLiteCursorDriver masterQuery, String editTable, SQLiteQuery query) {
                    return null;
                }
            };
        }

        /*open the database, if non exist, create one according to protocol*/
        public void initiate_database(){
            //open/create the database
            _database = SQLiteDatabase.openOrCreateDatabase("FDB", _cursorFactory);

            //TODO:: add support to Image and media handeling. as presented here: http://stackoverflow.com/questions/9357668/how-to-store-image-in-sqlite-database
            /*open/create tables*/
            //mainlist's dataList table
            _database.execSQL("CREATE TABLE IF NOT EXISTS dataList(Title VARCHAR, Album VARCHAR, Artist VARCHAR, Year VARCHAR, Genres VARCHAR, ID INTEGER);");
            //playlists table
            _database.execSQL("CREATE TABLE IF NOT EXIST playlists(Title VARCHAR, ID INT,key INTEGER, foreign_key VARCHAR, isAlbum INTEGER");

        }

        public class Reader extends FrontManager{
            //TODO:: Reader class

            /*constructor*/
            public Reader(){
                super(); // calling parent's constructor
            }

            /*fetches the values for the class fields fron the front database*/
            private void initiate_fields(){
                //fetch entire dataList table
                Cursor resultSet = _database.rawQuery("Select * from dataList",null);


                for(int i = 0; i < resultSet.getCount(); i++){
                    _titles.set(i, resultSet.getString(0));

                    _artists.set(i, resultSet.getString(2));

                }

                //releasing cursor
                resultSet.close();
            }

            /*will read the database for the table with the given ID, if found will create a PlayList object and will return it, if not found will return NULL*/
            public PlayList get_list_by_id(int id){
                Cursor results = _database.rawQuery("SELECT * FROM playlists WHERE ID=" + Integer.toString(id), null); // request all playlists that match the given id (there is only one)

                //results(1) is the id of the playlist and results(0) is its title
                String playlist_title = results.getString(0);
                long playlist_id = Long.parseLong(results.getString(1));

                PlayList pls = new PlayList(new ArrayList<Song>(),playlist_id, playlist_title);

                //read the database for the table with the name of the playlist id (the name of a datalist table is its ID)
                results = _database.rawQuery("SELECT * FROM" + playlist_id, null);

                /*getCount() returns the number of rows in the DB - the number of songs*/
                for(int i = 0; i < results.getCount(); i++){
                    // extract a song id in each loop and search the mainlist for the song it belongs to, then push it the playlist
                    pls.push(Utility.Locator.get_song_by_id(_mainlist, Long.parseLong(results.getString(1))));
                }

                //release the Cursor
                results.close();

                //return the playlist
                return pls;
            }
            //TODO:: OTHER SEARCH OPTIONS (OTHER THEN ID)

            /*will return all the playlists saved in the front database, returns them as a PlayList object inside of an ArrayList.*/
            public ArrayList<PlayList> get_playlists(){
                //TODO:: get_playlists()
                return null; // temp
            }



        }

        /*singleton class, responsible for writing to front database*/
        public class Writer extends FrontManager{

            /*constructor, made private so that only one instance of class will be able to exist at one time*/
            private Writer(){
                super(); // calling parent's constructor
            }

            /*since constructor is private you will be able to get an instance of class from here*/
            public boolean getInstance(){
                if(instance == null) {
                    instance = new Writer();
                    return true;
                } else return false;
            }

            /*writes the playlist to the front database: creates a new table for the data (datalist) and adds the field to the playlists table*/
            public void add_playlist(PlayList pls){
                //TODO: add_playlist()
            }

            /*adds the given song to the given playlist*/
            public void add_song_to_playlist(PlayList pls, Song s){
                //TODO:: add_song_to_playlist()
            }

            /*will reset front database and will put the back database's default values of the songs (saved in the mp3 metadata)*/
            public void reset_to_metadata()
            {
                //TODO:: reset_to_metadata()
                //NOTE:: _mainlist already has the metadata values saved inside of its Song object from the original query. so just update the database according to the values in there.D
            }

            /*will delete the given PlayList from the database*/
            public void delete_playlist(PlayList pls){
                //TODO:: delete_playlist()
            }

            /*will delete a song from the front database: the song will still exist on the back database but will not be shown to the user and will not be saved in the front database.*/
            public void delete_song(Song s){
                //TODO:: delete_song()
            }

            /*will change the details of the "old" to the details of "s"*/
            public void modify_song(Song s, Song old){ // when called: use Utility.Constants.KEEP_SAME to keep fields as they were
                //TODO:: modify_song()
            }

            /*will change the details of the "old" playlist to the details of the "pls"*/
            public void modify_playlist(PlayList pls, PlayList old){ // when called: use Utility.Constants.KEEP_SAME to keep fields as they were
                //TODO:: modify_playlist()
            }
        }
    }
}

//FIXME:: a logical error::  when deleting a song from the front database, how will we ensure that in the next front database update it will not be shown again (since the song is still kept at the back database)
