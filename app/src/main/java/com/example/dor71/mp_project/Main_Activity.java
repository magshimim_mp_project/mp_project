package com.example.dor71.mp_project;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

@TargetApi(23) // for debugging purposes

public class Main_Activity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 90;
    private PlayList mainList; // the main song list
    MediaController mdc;
    Seeker skr;
    GUI gui;

    boolean pp_flag; // true = play ; false = pause
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);
        pp_flag = false;
        gui = new GUI(this);

        mainList = new PlayList(DatabaseManager.BackManager.get_songs(getContentResolver()));
        Utility.Sorter.alpha_sort_by_name(mainList); // sprt list alphabetically
        listView_adapter();
        mdc = new MediaController(mainList.get_data(), gui);
        skr = new Seeker(mdc.get_mp());

        request_premission();

        //creating listeners
        createListeners();

        //start a thread that will update seeker and other values in real time
        new Thread(new Realtime_worker(gui, mdc)).start();

    }

    /*creating all the listeners for the UI*/
    private void createListeners() {
        //setting on click listeners for the media buttons
        gui.get_pp_bttn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pp_flag) { // if playing
                    pp_set_pause();
                    mdc.pause(); // pause song

                } else { // if not playing
                    pp_set_play();
                    mdc.resume(); //resume song
                }
            }
        });
        gui.get_forward_bttn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(gui.get_shuffle_checkbox().isChecked()){mdc.play_rand_song();} else{mdc.play_next();} //play next song
                } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        });
        gui.get_backward_bttn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(gui.get_shuffle_checkbox().isChecked()){mdc.play_rand_song();} else{mdc.play_prev();} //play previous song

                } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        });
        gui.get_mainList_view().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) { // play chosen song
                Song s = (Song) parent.getItemAtPosition(position);
                try {
                    getApplicationContext();
                    mdc.play_song(s, getApplicationContext());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                pp_set_play();
            }
        });
        gui.get_seeker().setOnSeekBarChangeListener(new Seeker(mdc.get_mp())); // seek to chosen location
    }


    /*pause the player when the user leaves the application*/
    protected void onPause() //NOTE: this is just a temporary function, will not be used in the final version
    {
        super.onPause();
        try {
            mdc.pause();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    //TODO:: add onStop function


    /*link main playlist to the list view*/
    private void listView_adapter() {

        ArrayAdapter<Song> listview_adapter = new list_adapter(Main_Activity.this, android.R.layout.simple_list_item_1, mainList.get_data());
        gui.get_mainList_view().setAdapter(listview_adapter);
    }

    private void pp_set_play() {
        pp_flag = true;
        gui.get_pp_bttn().setBackgroundResource(R.drawable.pause_bttn_img); // change button icon to pause button

    }

    private void pp_set_pause() {
        pp_flag = false;
        gui.get_pp_bttn().setBackgroundResource(R.drawable.play_bttn_img); // change button icon to play button

    }

    private void request_premission(){
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // Explain to the user why we need to read the contacts
                Toast.makeText(getApplicationContext(), "plz?", Toast.LENGTH_LONG).show();
            }

            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            // MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE is an
            // app-defined int constant that should be quite unique
        }

    }

}
