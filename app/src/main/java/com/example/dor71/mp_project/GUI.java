package com.example.dor71.mp_project;

import android.app.Activity;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by dor71 on 2/7/2017.
 */

/*will group up all of the gui elements for ease of handle*/
public class GUI {
   private Activity activity;

    //Buttons
    private Button _pp_bttn, _forward_bttn, _backward_bttn;
    private ListView _mainList_view; // the main song list view
    private SeekBar _seeker;
    private TextView _currtime_lable, _maxtime_lable;
    private TextView _curr_song_title;
    private CheckBox _shuffle_checkbox;

    public GUI(Activity activity) {
        this.activity = activity;
        //linking buttons and list to view
        _pp_bttn = (Button)this.activity.findViewById(R.id.play_pause_bttn);
        _forward_bttn = (Button)this.activity.findViewById(R.id.forward_bttn);
        _backward_bttn = (Button)this.activity.findViewById(R.id.backward_bttn);
        _mainList_view = (ListView)this.activity.findViewById(R.id.mainList_view);
        _seeker = (SeekBar)this.activity.findViewById(R.id.seeker);
       // _currtime_lable = (TextView)this.activity.findViewById(R.id.currtime_lable); //TODO: once added and fixed uncomment
       // _maxtime_lable = (TextView)this.activity.findViewById(R.id.maxtime_lable);
        _curr_song_title = (TextView)this.activity.findViewById(R.id.title_lable);
        _shuffle_checkbox = (CheckBox)this.activity.findViewById(R.id.shuffle_checkbox);
    }

    /*getters*/
    public Button get_pp_bttn() {
        return _pp_bttn;
    }
    public Button get_forward_bttn() {
        return _forward_bttn;
    }
    public Button get_backward_bttn(){
        return _backward_bttn;
    }
    public ListView get_mainList_view(){
        return _mainList_view;
    }
    public SeekBar get_seeker(){
        return _seeker;
    }
    public TextView get_currtime_lable(){
        return _currtime_lable;
    }
    public TextView get_maxtime_lable(){
        return _maxtime_lable;
    }
    public Activity get_activity(){return activity;}
    public TextView get_curr_song_title(){return _curr_song_title;}
    public CheckBox get_shuffle_checkbox(){return _shuffle_checkbox;}

    //TODO: changers and modifiers, then update other classes accordingly

    /*setters*/
    public void set_pp_bttn(Button b){ _pp_bttn = b;}
    public void set_forward_bttn(Button b){ _forward_bttn = b;}
    public void set_backward_bttn(Button b){ _backward_bttn = b;}
    public void set_mainList_view(ListView lstv){_mainList_view = lstv;}
    public void set_seeker(SeekBar sb){_seeker = sb;}
    public void set_shuffle_checkbox(CheckBox cb){_shuffle_checkbox = cb;}
    public void set_curr_song_title(TextView tv){_curr_song_title = tv;}
    public void set_currtime_lable(TextView tv){_currtime_lable = tv;}
    public void set_maxtime_lable(TextView tv){_maxtime_lable = tv;}

}