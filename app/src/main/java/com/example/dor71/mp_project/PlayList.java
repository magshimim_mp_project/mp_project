package com.example.dor71.mp_project;

import android.app.ListActivity;
import android.media.Image;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by dor71 on 12/20/2016.
 */

/*will be the universal way to present song l*/
public class PlayList extends ListActivity
{
     private ArrayList<Song> _data;
    private long _id;
    private String _name;

    /*constructor*/
    public PlayList(ArrayList<Song> data, long id, String name){
        _data = data;
        _id = id;
        _name = name;
    }
    /*copy constructor*/
    public PlayList(PlayList playList){
        this._data = playList.get_data();
        this._id = playList.get_id();
        this._name = playList.get_name();
    }

    /*getters + setters*/
    public long get_id(){
        return _id;
    }
    public void set_id(long id){
        _id = id;
    }
    public String get_name(){
        return _name;
    }
    public void set_name(String name){
        _name = name;
    }
    public ArrayList<Song> get_data(){
        return _data;
    }
    public void set_data(ArrayList<Song> lst){
        _data = lst;
    }

    /*adds a new song to the playlist*/
    public void push(Song s){
        _data.add(s);
    }

    /*removes a chosen song from the playlist*/
    public void remove(Song s){
        _data.remove(s);
    }

    /*searches the playlist for a song by id*/
    public Song find_song_by_id(long id){
        for(int i = 0; i < _data.size(); i++){
            if(_data.get(i).get_id() == id) {
                return _data.get(i);
            }
        }
        return null;
    }

    /*searches the playlist for a song by a Song object*/
    public Song find_song_by_object(Song s) {
        for(int i = 0; i < _data.size(); i++){
            if(_data.get(i) == s) {
                return _data.get(i);
            }
        }
        return null;
    }

    /*searches the playlist for a song by name*/
    public Song find_song_by_name(String str){
        for(int i = 0; i < _data.size(); i++){
            if(_data.get(i).get_title().equals(str)) {
                return _data.get(i);
            }
        }
        return null;
    }

    /*returns the song that comes after the given song in the playlist*/
    public Song get_next_song(Song s)
    {
        return Utility.Locator.get_next_song(new PlayList(_data, 0, ""), s);
    }

    /*returns the song that came before the given song in the playlist*/
    public Song get_previous_song(Song s)
    {
        return Utility.Locator.get_previous_song(new PlayList(_data, 0, ""), s);
    }

    /*returns an arrayList of all the song titles in the playlist*/
    public ArrayList<String> get_names()
    {
        ArrayList<String> s = new ArrayList<>();
        for(int i = 0; i < _data.size(); i++)
        {
            s.add(_data.get(i).get_title());
        }
        return s;
    }


    public class Album extends PlayList{
        Image _img;

        /*constructor*/
        public Album(ArrayList<Song> data, long id, String name){
            super(data, id, name); // calling the superclass's (Playlist) constructor

        }

    }
}
