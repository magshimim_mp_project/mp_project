package com.example.dor71.mp_project;

import android.media.Image;
import android.net.rtp.AudioStream;
import android.provider.MediaStore;

import java.io.FileInputStream;

/**
 * Created by dor71 on 12/20/2016.
 */

/*represents a single song.*/
public class Song
{
    //basic fields
    private long _id;
    private String _title, _artist, _album, _year;
    private Image _image;
    FileInputStream _file;


    /*constructor*/
    public Song(long id, String title, String artist, String album, String year, Image img, FileInputStream file){
        _id = id;
        _title = title;
        _artist = artist;
        _album = album;
        _year = year;
        _image = img;
        _file = file;
    }

    /*getters + setters*/
    public long get_id(){ // id getter
        return _id;
    }
    public void set_id(long id){ // id setter
        _id = id;
    }
    public String get_title(){ // title getter
        return _title;
    }
    public void set_title(String title){ // title setter
        _title = title;
    }
    public String get_artist(){
        return _artist;
    }
    public void set_artist(String artist){
        _artist = artist;
    }
    public String get_album(){
        return _album;
    }
    public void set_album(String album){
        _album = album;
    }
    public String get_year(){
        return _year;
    }
    public void set_year(String year){
        _year = year;
    }
    public Image get_image()
    {
        return _image;
    }
    public void set_image(Image image){
        _image = image;
    }
    public FileInputStream get_file(){
        return _file;
    }
    public void set_file(FileInputStream file){
        _file = file;
    }

}
