package com.example.dor71.mp_project;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by dor71 on 2/7/2017.
 */

//FIXME:: seekbar starts at the end instead of the beginning 7.2.17

/*will update and make changes in realtime*/
public class Realtime_worker implements Runnable {

    GUI _gui;
    MediaController _mdc;
    SeekBar _skbr;
    TextView _max, _curr;
    Handler handler;

    /*constructor*/
    public Realtime_worker(GUI gui, MediaController mdc) //TODO: add support for the timing labels
    {
        handler = new Handler();
        _skbr = gui.get_seeker();
        _mdc = mdc;
        _curr = gui.get_currtime_lable();
        _max = gui.get_maxtime_lable();
        _gui = gui;
    }

    @Override
    public void run() // will update seekbar every 1000 milliseconds to the current song position
    {
        Looper.prepare();
        _skbr.setProgress(0);
        //will loop as long as a song is playing
        while (true) if (_mdc.isPlaying()) {
            try {
                Thread.sleep(1000); // wait for 1000 milliseconds before updating again
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
                /*handler.post(new Runnable(){ //FIXME: crushes dou to Resources$NotFoundException
                    @Override
                    public void run(){
                        update_timers();
                    }
                });*/
            update_seeker(); // update the seeker position
        }

    }
    /*updates seeker to its current position in the song*/
    private void update_seeker() {
        int temp = _mdc.get_mp().getCurrentPosition();
        //TODO: update timing labels here
        if(temp != 0){
            _skbr.setProgress(temp); // seek to current position
        }
        if(_skbr.getProgress() == _skbr.getMax()) { //FIXER: in order to fix the bug which the seekbar gets stuck at the end, if it is at the end and the song is still playing return it to the start
            _skbr.performClick();
        }
    }

    /*updates timing lables to the current time of the song*/
    private void update_timers() //FIXME: bugged, crushes application (see Realtime_worker.run())
    {
        _max.setText(_mdc.get_mp().getDuration());
        _curr.setText(_mdc.get_mp().getCurrentPosition());
    }
}
