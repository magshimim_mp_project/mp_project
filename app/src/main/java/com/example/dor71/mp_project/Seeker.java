package com.example.dor71.mp_project;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.SeekBar;

/**
 * Created by dor71 on 1/27/2017.
 */

public class Seeker implements /*Runnable,*/ SeekBar.OnSeekBarChangeListener{

    MediaPlayer _mp;
    int prog;

    /*constructor*/
    public Seeker(MediaPlayer mp){
        _mp = mp;
    }

    @Override
    public void onProgressChanged(SeekBar _progbar, int progress, boolean fromUser)
    {
        if(fromUser)
        {
            prog = progress;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar _progbar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar _progbar) {
        _progbar.setMax(_mp.getDuration());
        _mp.seekTo(prog);
    }
}
