package com.example.dor71.mp_project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by dor71 on 1/23/2017.
 */

/*an adapter to connect the listview_item xml to the main listview*/
public class list_adapter extends ArrayAdapter<Song> {
    PlayList _data;
    public list_adapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    /*constructor*/
    public list_adapter(Context context, int resource, ArrayList<Song> items) {
        super(context, resource, items);
    }


    @Override /*will define he view of each item on the listview, will be called for each item on the list*/
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        //calling the listview_item xml
        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.listview_item, null);
        }

        Song p = getItem(position); // checking which item on the list is *this*

        // setting the text of the text view to the song title
        if (p != null) {
            TextView tv = (TextView) v.findViewById(R.id.song_title);
            if (tv != null) {
                tv.setText(p.get_title());
            }

        }

        return v;
    }


}
